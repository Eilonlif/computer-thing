import pathlib
with open('contents.txt', 'w') as f:
    f.write('')

folder = pathlib.Path('.')
for file_or_dir in folder.iterdir():
    if not(file_or_dir.is_dir()) and not str(file_or_dir).startswith("."):
        print(str(file_or_dir))
        with open(str(file_or_dir), 'r') as a:
            text = a.read()
        with open('contents.txt', 'a') as b:
            b.write(text)
